import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class SettingsService
{
	timer_minutes: number = 2 		// unit in minutes

	/**
	 * For debugging or designing, to stop api requests to hit, use this variable as True/False
	 * True: will stop the api hits.
	 * False: will allow the server to make an api request.
	 */
	STOPPED_API_REQUESTS: boolean = false
	LOG_CONSOLES: boolean = true


	constructor() { }

	color_pallete: any = {
		style_1: {
			dark: "#474448",
			light: "#2D232E",
			mid: "#E0DDCF",
			tone: "#534B52",
			g: "#F1F0EA"
		}
	}

	images: any = {
		project_logo: 'assets/images/icons/AuttyLogoText.png',
		project_logo_icon: 'assets/images/icons/AuttyLogo.png',
		lock: 'assets/images/lock.png'
	}
}
