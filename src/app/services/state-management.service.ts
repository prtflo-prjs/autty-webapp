import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';



@Injectable({
	providedIn: 'root'
})
export class StateManagementService
{

	errors = new BehaviorSubject("")

	constructor() { }

	/**
	 * updates into session storage based on state of prev stored value. If previous doesnt exist, saved as string
	 * Example:
	 * if previous value is array, updates into array.
	 * if previuos value is object, updates into object,
	 * if previous value is string, updates the string. Default.
	 * @param key_name: String [Required] name of the key to save the session with
	* @param value: Any [Required] any value to save
	 */
	setSession(key_name: string, value: any)
	{
		// check for a previous saved session with same name
		if (sessionStorage.getItem(key_name) != null)
		{
			let prev_session = JSON.parse(sessionStorage.getItem(key_name) ?? '')
			// if prev_session is object ?
			if (!Array.isArray(prev_session) && Object.keys(prev_session).length > 0)
			{
				// update into object
				sessionStorage.setItem(key_name, JSON.stringify(Object.assign(prev_session, value)))
			}
			// if prev_session is array ?
			else if (Array.isArray(prev_session))
			{
				// update into array
				prev_session.push(value)
				sessionStorage.setItem(key_name, JSON.stringify(prev_session))
			}
			// if string
			else if (typeof prev_session == 'string' || prev_session instanceof String)
			{
				// update into string
				sessionStorage.setItem(key_name, JSON.stringify(value))
			}
		} else
		{
			// default is string
			sessionStorage.setItem(key_name, JSON.stringify(value))
		}
	}


	getSession(name: string)
	{
		return JSON.parse(sessionStorage.getItem(name) ?? '')
	}


	setErrors(error: string)
	{
		this.errors.next(error)
	}

	get getErrors()
	{
		return this.errors
	}
}
