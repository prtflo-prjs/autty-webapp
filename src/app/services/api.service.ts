import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, catchError, retry, from } from 'rxjs';


import { SettingsService } from './settings.service';
import { StateManagementService } from './state-management.service';

@Injectable({
	providedIn: 'root'
})
export class ApiService
{
	logs: boolean = this.$settings.LOG_CONSOLES


	constructor(
		private $http: HttpClient,
		private $settings: SettingsService,
		private $stateManager: StateManagementService
	) { }

	/**
	 * A Common service ground to send the api requests
	 * @param request_type [Required] type of request to send
	 * @param url [Required] where to send the request
	 * @param data [Optional] data to pass with the request
	 * @returns
	 */
	sendRequest(request_type: string, url: string, data?: any): Observable<Record<string, any> | []>
	{
		if (request_type == 'get')
		{
			console.log('\n\nSending GET Request ...')
			return this.$http.get(url)
		}
		if (request_type == 'post')
		{
			console.log('\n\nSending POST Request ...')
			return this.$http.post(url, data)
		}
		if (request_type == 'put')
		{
			console.log('\n\nSending PUT Request ...')
			return this.$http.put(url, data)
		}
		if (request_type == 'delete')
		{
			console.log('\n\nSending DELETE Request ...')
			return this.$http.delete(url, data)
		}

		// a default hit to server if no request type is specified.
		return this.$http.get('/')	// checks if the server is working
	}
}
