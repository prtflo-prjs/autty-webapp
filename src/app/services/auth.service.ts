import { LIVE_ANNOUNCER_DEFAULT_OPTIONS } from '@angular/cdk/a11y';
import { Injectable, ɵclearResolutionOfComponentResourcesQueue } from '@angular/core';
import { catchError, map, Observable, of, throwError, retry, from } from 'rxjs';

import { ApiService } from './api.service';
import { StateManagementService } from './state-management.service';

@Injectable({
	providedIn: 'root'
})
export class AuthService
{
	constructor(
		private $api: ApiService
	)
	{ }


	/**
	 * Fetch authentication token from Local Storage
	 * @returns <string> auth token retrieved from local storage
	 */
	getClientToken(): string
	{
		// return client token from local storage
		return (JSON.parse(localStorage.getItem('token') ?? '')).toString()
	}

	/**
	 * Fetch Client Id from Local Storage.
	 * @returns [string] client id retrieved from local storage.
	 */
	getClientId(): string
	{
		return (JSON.parse(localStorage.getItem('client') ?? '')).toString()
	}

	/**
	 * Perform User Logout Operation
	 */
	performUserLogOut()
	{
		localStorage.clear()
	}


	refreshAuthToken()
	{
		return new Observable()
	}

	/**
	 * Send the request to $api service for login credentials
	 * @params data [Required] data to send
	 */
	userLogin(data: any): Observable<Object>
	{
		return this.$api.sendRequest('post', 'creds/login', data)
	}


	userSignup(data: any): Observable<Object>
	{
		return this.$api.sendRequest('post', 'creds/signup', data)
	}
}
