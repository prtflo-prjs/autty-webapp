import { Inject, Injectable, Optional } from '@angular/core';
import { catchError, from, map, Observable, retry } from 'rxjs';
import { ApiService } from './api.service';
import { SettingsService } from './settings.service';
import { StateManagementService } from './state-management.service';

@Injectable({
	providedIn: 'root'
})
export class PasswordsService
{
	logs: boolean = this.$settings.LOG_CONSOLES

	clientId!: string

	constructor(
		private $api: ApiService,
		private $stateManager: StateManagementService,
		private $settings: SettingsService,
	)
	{
		this.clientId = localStorage.getItem('client') ?? ''
	}

	fetchAllPasswords(): Observable<Record<string, any>>
	{
		return this.$api.sendRequest('get', `client/${this.clientId}/sites/authpasses`)
	}
}
