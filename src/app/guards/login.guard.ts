import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SettingsService } from '../services/settings.service';

@Injectable({
	providedIn: 'root'
})
export class LoginGuard implements CanActivate
{
	constructor(
		private $router: Router,
		private $settings: SettingsService,
	) { }
	canActivate(
		_route: ActivatedRouteSnapshot,
		_state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree
	{
		console.log('Guard')
		return localStorage.getItem('client') !== null
	}

}
