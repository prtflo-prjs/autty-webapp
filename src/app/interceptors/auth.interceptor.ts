import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, catchError, filter, finalize, Observable, switchMap, take, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor
{
	private refreshTokenInProgress: boolean = false
	private $refreshToken$ = new BehaviorSubject(null)


	constructor(
		private $AuthService: AuthService
	) { }

	/**
	 *
	 * Learning Source: https://dev-academy.com/how-to-use-angular-interceptors-to-manage-http-requests/
	 *
	 */

	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>>
	{
		return next.handle(request)
		return next.handle(this.addAuthToken(request)).pipe(
			// to catch the error when user tries to request an api without authorization.
			// becuase this.addAuthToken() will return a request with no authorization header, the api will send a 401 (Unauthorized) error.
			catchError((requestError: HttpErrorResponse) =>
			{
				// how to handle unauthorized access ?
				if (requestError && requestError.status === 401)
				{
					// some additional checks to be done on how to proceed with the request
					if (this.refreshTokenInProgress)
					{
						// if a new refresh token is already being processed, the workflow will wait until it passes the new token to behavior subject and finally include in the header.
						return this.$refreshToken$.pipe(
							take(1),
							switchMap(() => next.handle(this.addAuthToken(request)))
						)
					}
					else
					{
						// what if there is no refresh token ?
						this.refreshTokenInProgress = true	// flag that a new request token in now in process
						this.$refreshToken$.next(null)	// ensure no unexpected value is pending on subject

						return this.$AuthService.refreshAuthToken().pipe(
							switchMap((token: any) =>
							{
								this.$refreshToken$.next(token)
								return next.handle(this.addAuthToken(request))
							}),
							// end the process clean
							finalize(() => this.refreshTokenInProgress = false)
						);
					}
				}
				else
				{
					// if not 401 error
					return throwError(() => new Error(requestError.message))
				}
			})
		);
	}


	addAuthToken(request: HttpRequest<any>)
	{
		const auth_token = this.$AuthService.getClientToken()

		if (!auth_token)
		{
			return request
		}

		return request.clone({
			setHeaders: {
				Authorization: `Bearer ${auth_token}`
			}
		})
	}
}
