import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, retry, map, catchError, throwError } from 'rxjs';
import { StateManagementService } from '../services/state-management.service';
import { SettingsService } from '../services/settings.service';
import { environment } from '../../environments/environment';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor
{
	AUTH_TOKEN = sessionStorage.getItem('returning_user') ?? 'guest'

	STOPPED_API_REQUESTS: boolean = this._settings.STOPPED_API_REQUESTS

	error: string = ''		// list of errors if any persists

	constructor(
		private _state: StateManagementService,
		private _settings: SettingsService
	) { }

	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>>
	{
		// for development purposes, to stop API requests
		if (this.STOPPED_API_REQUESTS)
		{
			console.log('STOPPED API REQUESTS')
			return new Observable()
		}
		request = request.clone({
			setHeaders: {
				// TOKEN: this.AUTH_TOKEN,
				'Content-Type': 'application/json',
				"Accept": 'application/json',
			},
			url: environment.api_url + request.url,
		})
		return next.handle(request).pipe(
			map((response: HttpEvent<any>) =>
			{
				console.log('Hitting the Backend Server ...')
				if (response instanceof HttpResponse)
				{
					console.log('\n\n\n')
					console.group(`Response from request: \n${response.url}`)
					console.group(response)
					console.groupEnd()
					console.log('\n\n\n')
				}
				return response
			}),
			catchError((err: any) =>
			{
				console.error('Error response ', err)
				if (err.error.server_error)
				{
					this.error = err.error.message
				} else if (err.status == 400 || err.status == 401)
				{
					this.error = 'Access Denied'
				} else if (err.status >= 500 && err.status <= 550)
				{
					this.error = 'Server Error'
				} else if (err.status == 422)
				{
					this.error = 'Validation Error.'
				} else
				{
					console.log(err)
					this.error = err.message
				}
				this._state.setErrors(this.error)
				return throwError(() =>
				{
					return new Error(this.error)
				})
			})
		)
	}
}
