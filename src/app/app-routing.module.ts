import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { PageNotFoundComponent } from './components/dashboard/page-not-found/page-not-found.component';
import { LoginGuard } from './guards/login.guard';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'authenticate',
		pathMatch: 'full'
	},
	{
		path: 'authenticate',
		component: AuthComponent,
	},
	{
		path: 'user',
		loadChildren: () => import('./components/dashboard/dashboard.module').then(m => m.DashboardModule),
	},
	{
		path: 'logout',
		component: LogoutComponent
	},
	{
		path: '**',
		component: PageNotFoundComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
