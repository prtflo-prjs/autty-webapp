import { Component, ElementRef, ViewChild } from '@angular/core';

import { StateManagementService } from './services/state-management.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass']
})
export class AppComponent
{
	title = 'Authanticator'

}
