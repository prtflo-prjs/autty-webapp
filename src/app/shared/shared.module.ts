import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from '../components/dashboard/tabs/tabs.component';
import { HeaderComponent } from '../components/header/header.component';
import { RouterModule } from '@angular/router';
import { GlobalModalComponent } from '../components/global-modal/global-modal.component';
import { MessageBoxComponent } from '../components/message-box/message-box.component';
import { ErrorModalComponent } from '../components/error-modal/error-modal.component';





@NgModule({
	declarations: [
		TabsComponent,
		HeaderComponent,
		GlobalModalComponent,
		MessageBoxComponent,
		ErrorModalComponent
	],
	imports: [
		CommonModule,
		RouterModule,
	],
	exports: [
		TabsComponent,
		HeaderComponent,
		GlobalModalComponent,
		ErrorModalComponent
	]
})
export class SharedModule { }
