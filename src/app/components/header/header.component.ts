import { Component, OnInit, HostListener, Input } from '@angular/core';

import { SettingsService } from '../../services/settings.service'


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit
{
    @Input() screen: string = 'default'
    navs: boolean = true
    settings = this._settings


    screenWidth = window.innerWidth
    constructor(
        private _settings: SettingsService
    ) { }

    ngOnInit(): void
    {
        if (this.screen === 'auth')
        {
            this.navs = false
        }
    }

    @HostListener('window:resize', ['$event'])
    onWindowResize()
    {
        this.screenWidth = window.innerWidth
    }

}
