import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-tabs',
	templateUrl: './tabs.component.html',
	styleUrls: ['./tabs.component.sass']
})
export class TabsComponent implements OnInit
{
	tabinfo: Array<any> = ['empty']
	user_id!: Number
	constructor(
		private _activatedRoute: ActivatedRoute
	) { }

	ngOnInit(): void
	{
		if (sessionStorage.getItem('client') != null)
		{
			this.user_id = JSON.parse(sessionStorage.getItem('client') ?? '')['user_id']
		}

		// to generate tab content via routing file with values in 'data' field
		if (this.tabinfo !== undefined && this.tabinfo.length == 0)
		{
			this._activatedRoute.data.subscribe((data: any) =>
			{
				this.tabinfo = data.tabinfo
			})
			console.log('Tab Info Data: ', this.tabinfo)
		}
	}

}
