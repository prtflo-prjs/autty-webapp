import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Material Design Modules
import { MatDialogModule } from '@angular/material/dialog';

// Custom Components
import { TabsComponent } from './tabs/tabs.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { PasswordsComponent } from './passwords/passwords.component';


@NgModule({
	declarations: [
		DashboardComponent,
		BankDetailsComponent,
		PasswordsComponent
	],
	imports: [
		CommonModule,
		RouterModule,
		SharedModule,
		DashboardRoutingModule,
		MatDialogModule,
	],
	providers: [
	],
	exports: [
		RouterModule,
	],
	bootstrap: [],
})
export class DashboardModule { }
