import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

// Guards
import { LoginGuard } from "src/app/guards/login.guard";

// Components
import { DashboardComponent } from "./dashboard.component";
import { TabsComponent } from "./tabs/tabs.component";
import { BankDetailsComponent } from "./bank-details/bank-details.component";
import { PasswordsComponent } from "./passwords/passwords.component";


const routes: Routes = [
	{
		path: '',
		redirectTo: 'dashboard',
		pathMatch: 'full'
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
		// canActivate: [LoginGuard],
		children: [
			{
				path: '',
				component: TabsComponent,
			},
			{
				path: 'passwords',
				component: PasswordsComponent
			},
			{
				path: 'bank-details/:user_id',
				component: BankDetailsComponent
			}
		]
	},
];

@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	]
})
export class DashboardRoutingModule
{
}
