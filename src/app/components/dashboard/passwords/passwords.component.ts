import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { PasswordsService } from 'src/app/services/passwords.service';

import { SettingsService } from 'src/app/services/settings.service';


@Component({
	selector: 'app-passwords',
	templateUrl: './passwords.component.html',
	styleUrls: ['./passwords.component.sass'],
})
export class PasswordsComponent implements OnInit
{
	@ViewChild('dialogbox') dialogbox!: ElementRef
	@ViewChild('time') time!: ElementRef

	modal_options: any


	settings = this._settingsService
	is_modal_active: boolean = false
	close_eye: boolean = false
	is_text_copied: boolean = false


	constructor(
		private _settingsService: SettingsService,
		private $api: PasswordsService,
	) { }

	ngOnInit(): void
	{
		this.modal({
			html_content: `You have <strong>${this.settings.timer_minutes}</strong> minutes to work on this page. After the time, you will be redirected to dashboard page. <br> Would you like to continue ?`,
			p_text: 'Continue',
			s_text: 'Cancel',
			modal_title: 'Session Note'
		}).hide()
		this.StartTimer(--this._settingsService.timer_minutes, 10)		// start the timer with 59 seconds and given minutes - 1

		this.fetchAllPasswords()
	}


	/**
	 * Timer to be displayed at the start of the session.
	 * @param minutes number of minutes
	 * @param seconds number of seconds
	 */
	StartTimer(minutes: number, seconds: number = 59)
	{
		const interval_func = setInterval(() =>
		{
			if (seconds == 0 && minutes == 0)
			{
				clearInterval(interval_func)
				this.time.nativeElement.innerHTML = "Session Expired"
				return
			}
			if (seconds == 0)
			{
				this.StartTimer(--minutes)
			} else
			{
				--seconds
			}
			// for countdown to show (if in future)
			// let days = Math.floor((time_left / (24 * 60 * 60 * 1000)))
			// let hours = Math.floor((time_left % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000))
			// let minutes = Math.floor((time_left % (60 * 60 * 1000)) / (60 * 1000))
			// let seconds = Math.floor((time_left % (60 * 1000)) / 1000)

			this.time.nativeElement.innerHTML = `${minutes < 10 ? '0' : ''}${minutes}m : ${seconds < 10 ? '0' : ''}${seconds}s`
		}, 1100)
	}


	/**
	 * Adds three dots at the end of the string which exceeds the given limit.
	 * @param text: String [Required] text to truncate
	 * @param limit: Number [Required] limit to reach
	 *
	 * @returns String
	   */
	addThreeDots(text: string, limit: number): string
	{
		const dots = ' ...'
		if (text.length > limit)
		{
			return text.substring(0, limit + 1) + dots
		}
		return text
	}

	modal(options: any)
	{
		this.modal_options = options
		return {
			show: () =>
			{
				this.is_modal_active = true
			},
			hide: () =>
			{
				this.is_modal_active = false
			}
		}
	}

	isSuccess(event: any)
	{
		console.log(event)
	}

	copyToClipboard(text: string)
	{
		setTimeout(() =>
		{
			this.is_text_copied = false
		}, 2000)

		// navigator clipboard api needs a secure context (https)
		if (navigator.clipboard && window.isSecureContext)
		{
			// navigator clipboard api method'
			this.is_text_copied = true
			return navigator.clipboard.writeText(text);
		} else
		{
			// text area method
			let textArea = document.createElement("textarea");
			textArea.value = text;
			// make the textarea out of viewport
			textArea.style.position = "fixed";
			textArea.style.left = "-999999px";
			textArea.style.top = "-999999px";
			document.body.appendChild(textArea);
			textArea.focus();
			textArea.select();
			return new Promise((res, rej) =>
			{
				// here the magic happens
				document.execCommand('copy') ? this.is_text_copied = true : console.log('Could not copy text');
				textArea.remove();
			});
		}
	}


	fetchAllPasswords()
	{
		return new Promise((resolve, reject) =>
		{
			this.$api.fetchAllPasswords().subscribe((response: any) =>
			{
				console.log(response)
				// resolve(response)
			})
		})
	}
}
