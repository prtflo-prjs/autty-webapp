import { Component, ElementRef, OnChanges, SimpleChanges, Input, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

interface ModalOptions
{
	html_content?: string,
	s_text?: string,
	p_text?: string,
	modal_title?: string
}

@Component({
	selector: 'global-modal',
	templateUrl: './global-modal.component.html',
	styleUrls: ['./global-modal.component.sass']
})
export class GlobalModalComponent implements OnInit
{
	@ViewChild('globalModal') g_modal!: ElementRef;

	@Input('activate_modal') modal: any
	@Output() success = new EventEmitter<boolean>()

	@Input('options') options: ModalOptions = {}

	html_content: string = this.options.html_content || 'This is a note.'
	modal_title: string = this.options.modal_title || 'Note'
	primary_text: string = this.options.p_text || 'Okay'
	secondary_text: string = this.options.s_text || ''



	constructor()
	{ }

	ngOnInit(): void
	{
		console.log(this.modal)
	}

	ngOnChanges(change: SimpleChanges)
	{
		console.log('Changes: ', change)
		this.activateModal(change['modal'].currentValue)
		// 	if (change['modal'].firstChange)
		// 	{
		// 		this.activateModal(false)
		// 	} else
		// 	{
		// 		this.activateModal(true)
		// }
	}

	activateModal(is_show: boolean)
	{
		if (is_show)
		{
			document.getElementById('globalModal')?.classList.remove('hidden')
		} else
		{
			document.getElementById('globalModal')?.classList.add('hidden')
		}
	}

	isSuccess(bool: boolean): void
	{
		this.activateModal(false)
		if (bool)
		{
			this.success.emit(true)
		} else
		{
			this.success.emit(false)
		}
	}

}
