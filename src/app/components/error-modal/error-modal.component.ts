import { Component, OnInit, AfterViewInit, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { StateManagementService } from 'src/app/services/state-management.service';



@Component({
	selector: 'app-error-modal',
	templateUrl: './error-modal.component.html',
	styleUrls: ['./error-modal.component.sass']
})
export class ErrorModalComponent implements OnInit
{
	error_message: string = 'Test Message'

	@ViewChild('errorbox') errorbox!: ElementRef
	@ViewChild('errormodal', { static: true }) errormodal!: ElementRef



	constructor(
		private _state: StateManagementService,
		private _renderer: Renderer2
	) { }

	ngOnInit(): void
	{
		this._state.getErrors.subscribe((data: string) =>
		{
			console.log('Errors: ', data)
			this.error_message = data
			this.error_message != '' && this.showPopup(true)
		})
	}


	showPopup(is_show: boolean)
	{
		if (is_show)
		{
			this._renderer.removeClass(this.errormodal.nativeElement, 'hidden')
			this._renderer.removeClass(this.errormodal.nativeElement, 'error-modal-out-animation')
			this._renderer.addClass(this.errormodal.nativeElement, 'error-modal-in-animation')
		} else
		{
			// console.log(this.errorbox)
			this._renderer.removeClass(this.errorbox.nativeElement, 'error-modal-in-animation')
			this._renderer.addClass(this.errorbox.nativeElement, 'error-modal-out-animation')
			setTimeout(() =>
			{
				this._renderer.addClass(this.errormodal.nativeElement, 'hidden')
			}, 400)
		}
	}
}
