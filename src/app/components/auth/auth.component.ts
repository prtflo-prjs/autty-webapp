import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SettingsService } from 'src/app/services/settings.service';
import { StateManagementService } from 'src/app/services/state-management.service';

@Component({
	selector: 'app-auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.sass']
})
export class AuthComponent implements OnInit
{
	@ViewChild('errormessage') message!: ElementRef<HTMLInputElement>

	AuthenticationForm!: FormGroup

	// settings service
	settings = this.$settings

	is_signupform_active: boolean = false
	state: number = 0
	max_states: number = 3

	password: string = ""
	is_match_confpass: boolean = true

	private name_pattern = "[a-zA-Z ]+[^0-9]{5,25}"
	private email_pattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
	private password_pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&])[A-Za-z0-9@$!%*?&]{6,}$"
	private birth_year_pattern = "[0-9]+"



	constructor(
		private $settings: SettingsService,
		private $activeRoute: ActivatedRoute,
		private $router: Router,
		private $authService: AuthService,
		private $state: StateManagementService,
		private $renderer: Renderer2,
		private form_builder: FormBuilder
	) { }

	ngOnInit(): void
	{
		this.$activeRoute.queryParams.subscribe((item) =>
		{
			// just navigate to URL with query params
			if (Object.keys(item).length == 0)
			{
				// default navigate to guest always
				this.$router.navigate(['/authenticate'], { queryParams: { 'user-role': 'guest' } })
			}

			// condition for different roles
			if (item['user-role'] === 'admin')
			{
				sessionStorage.setItem('client', JSON.stringify({ 'user-role': 'admin' }))
				this.$router.navigateByUrl('/authenticate/admin/56')
			}
			else if (item['user-role'] === 'guest')
			{
				sessionStorage.setItem('client', JSON.stringify({ 'user-role': 'guest' }))
			} else
			{
				// if no user is matched, always navigate to guest
				this.$router.navigate(['/authenticate'], { queryParams: { 'user-role': 'guest' } })
			}
		})

		this.buildAuthenticationForm()
	}
	// ngOnInit ends

	buildAuthenticationForm()
	{
		return new Promise((resolve) =>
		{
			this.AuthenticationForm = this.form_builder.group({
				name: [''],
				email: [''],
				username: ['', Validators.required],
				password: ['', Validators.required],
				birth_year: ['1999']
			})
			resolve(TextTrackCue)
		})
	}

	SetFormValue(form_control: string, value: any)
	{
		console.log('Filling value of ' + form_control + ' as ' + value)
		this.AuthenticationForm.get(form_control)?.setValue(value)
		this.AuthenticationForm.updateValueAndValidity()
	}

	get Form()
	{
		return this.AuthenticationForm.controls
	}

	changeForm()
	{
		this.is_signupform_active = !this.is_signupform_active      // change data-bound value (toggle state)
		this.state = this.is_signupform_active ? 1 : 0

		if (this.is_signupform_active)
		{
			// set validations in form
			this.Form['name'].setValidators([Validators.required, Validators.pattern(this.name_pattern)])
			this.Form['email'].setValidators([Validators.required, Validators.pattern(this.email_pattern)])
			this.Form['username'].clearValidators()
			this.Form['password'].clearValidators()
			this.Form['password'].setValidators([Validators.required, Validators.pattern(this.password_pattern)])
			this.Form['birth_year'].setValidators([Validators.required, Validators.pattern(this.birth_year_pattern)])
		}
		else
		{
			this.Form['name'].clearValidators()
			this.Form['email'].clearValidators()
			this.Form['birth_year'].clearValidators()
			this.Form['username'].setValidators([Validators.required])
			this.Form['password'].setValidators([Validators.required, Validators.pattern(this.password_pattern)])
		}

		// always reset the values when form changes
		this.Form['username'].reset()
		this.Form['password'].reset()

		return
	}

	changeState(reverse?: boolean)
	{
		if (!reverse && this.state == this.max_states)
		{
			this.userSignup()
			return
		}

		if (reverse)
		{
			this.state -= 1
			return
		}
		console.log(this.AuthenticationForm)
		this.state += 1

	}


	checkAuthLoginCreds()
	{
		console.log('Sending Request to checkAuthLoginCreds, data: ', this.AuthenticationForm)

		// if username contains an email address
		if (this.Form.username.value && this.Form.username.value.indexOf('@') !== -1)
		{
			this.SetFormValue('email', this.Form.username.value)
		}

		if (this.AuthenticationForm.invalid)
		{
			console.log('Invalid Form: ', this.AuthenticationForm)
			console.log((this.Form.username.value == '' || this.Form.email.value == '') || this.Form.password.value == '')
			// display message if necessary details are not provided while login
			if ((this.Form.username.value == '' || this.Form.email.value == '') || this.Form.password.value == '')
			{
				this.displayMessage("Please provide necessary details. ")
			}
			return
		}
		this.$authService.userLogin(this.AuthenticationForm.value).subscribe((response: any) =>
		{
			if (response.message && response.data.length == 0)
			{
				this.$state.setErrors(response.message)
			}
			if (response.data.length > 0 && response.data[0].hasOwnProperty('token'))
			{
				this.$state.setSession('client', response[0])
				this.$router.navigate(['user'], {
					queryParams: {
						'logged-in': 'true'
					},
					queryParamsHandling: 'merge'
				})
				return
			}
		})
	}

	/**
	 * Signup the user by capturing his particulars.
	 * @returns navigations to login page.
	 */
	userSignup()
	{
		console.log("Sending Request to userSignup, data: ", this.AuthenticationForm.value)

		// fill the username when email is filled by capturing the address only while signing up
		if (this.Form.email.value)
		{
			this.SetFormValue('username', this.Form.email.value.slice(0, this.Form.email.value.indexOf('@')))
		}

		if (this.AuthenticationForm.invalid)
		{
			console.error('Invalid Form: ', this.AuthenticationForm)
			this.displayMessage('Please provide necessary details by going back and completing each step.')
			return
		}

		this.$authService.userSignup(this.AuthenticationForm.value).subscribe((response: any) =>
		{
			console.log(response)

			if (response && !response.server_error)
			{
				location.reload()
			}
		})
	}


	displayMessage(text: string)
	{
		console.log('Displaying Message')
		// display message and animation
		this.$renderer.setProperty(this.message.nativeElement, 'innerHTML', text)
		this.$renderer.addClass(this.message.nativeElement, 'error-animation')

		// remove message and animation
		setTimeout(() =>
		{
			this.$renderer.setProperty(this.message.nativeElement, 'innerHTML', '')
			this.$renderer.removeClass(this.message.nativeElement, 'error-animation')
		}, 2000)
	}

	checkConfirmPasswordMatch(value: string)
	{
		console.log(value)
		let password = this.Form.password.value

		if (password && password.indexOf(value) == -1)
		{
			this.is_match_confpass = false
			return
		}
		this.is_match_confpass = true
	}

}
