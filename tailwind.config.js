module.exports = {
  content: [
      "./src/**/*.{html,ts}"
  ],
  theme: {
    extend: {
        colors: {
            'orng': {
                100: '#FFD181',
                200: '#FFAB41',
                500: '#FF9201',
                800: '#FF6D00'
            },
            'silver': {
                100: '#f1f0ea',  // light-shade
				800: '#2b2a2a', // dark shade
            }
        }
    }
  },
  plugins: [],
}
